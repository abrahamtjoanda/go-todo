package repository

import (
	"context"
	"go-todo/internal/api/model/entity"

	"github.com/jackc/pgx/v5"
)

type TodoRepository interface {
	FindAll(ctx context.Context, tx pgx.Tx) []entity.Todo
	Create(ctx context.Context, tx pgx.Tx, item string) entity.Todo
	Delete(ctx context.Context, tx pgx.Tx, id int)
	Complete(ctx context.Context, tx pgx.Tx, id int) entity.Todo
}
