package repository

import (
	"context"
	"go-todo/internal/api/model/entity"
	"go-todo/internal/util"

	"github.com/jackc/pgx/v5"
)

type todoRepositoryImpl struct{}

func NewTodoRepository() TodoRepository {
	return &todoRepositoryImpl{}
}

func (repository todoRepositoryImpl) FindAll(ctx context.Context, tx pgx.Tx) []entity.Todo {
	sql := `select id, item, completed from todos order by id desc`
	rows, err := tx.Query(ctx, sql)
	util.PanicIfError(err)
	defer rows.Close()

	todos, err := pgx.CollectRows(rows, pgx.RowToStructByName[entity.Todo])
	util.PanicIfError(err)

	return todos
}

func (repository todoRepositoryImpl) Create(ctx context.Context, tx pgx.Tx, item string) entity.Todo {
	var todo entity.Todo
	sql := `insert into todos (item) values ($1) returning id, item, completed`
	err := tx.QueryRow(ctx, sql, item).Scan(&todo.Id, &todo.Item, &todo.Completed)
	util.PanicIfError(err)

	return todo
}

func (repository todoRepositoryImpl) Delete(ctx context.Context, tx pgx.Tx, id int) {
	sql := `delete from todos where id = $1`
	_, err := tx.Exec(ctx, sql, id)
	util.PanicIfError(err)
}

func (repository todoRepositoryImpl) Complete(ctx context.Context, tx pgx.Tx, id int) entity.Todo {
	var todo entity.Todo
	complete := true
	sql := `update todos set completed = $1 where id = $2 returning id, item, completed`
	err := tx.QueryRow(ctx, sql, complete, id).Scan(&todo.Id, &todo.Item, &todo.Completed)

	util.PanicIfError(err)

	return todo
}
