package handler

import (
	"net/http"
)

type TodoHandler interface {
	Show(writer http.ResponseWriter, request *http.Request)
	Create(writer http.ResponseWriter, request *http.Request)
	Delete(writer http.ResponseWriter, request *http.Request)
	Complete(writer http.ResponseWriter, request *http.Request)
}
