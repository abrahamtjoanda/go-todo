package handler

import (
	"go-todo/internal/api/service"
	"go-todo/internal/util"
	"html/template"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type todoHandlerImpl struct {
	TodoService service.TodoService
}

func NewTodoHandler(todoService service.TodoService) TodoHandler {
	return &todoHandlerImpl{
		TodoService: todoService,
	}
}

func (handler todoHandlerImpl) Show(writer http.ResponseWriter, request *http.Request) {
	todoResponses := handler.TodoService.FindAll(request.Context())

	t := template.Must(template.ParseFiles("./internal/web/templates/index.html"))
	_ = t.Execute(writer,
		map[string]any{
			"Todos": todoResponses,
		})
}

func (handler todoHandlerImpl) Create(writer http.ResponseWriter, request *http.Request) {
	item := request.FormValue("item")
	handler.TodoService.Create(request.Context(), item)

	http.Redirect(writer, request, "/", http.StatusMovedPermanently)
}

func (handler todoHandlerImpl) Delete(writer http.ResponseWriter, request *http.Request) {
	id := util.StringToInt(chi.URLParam(request, "id"))
	handler.TodoService.Delete(request.Context(), id)

	http.Redirect(writer, request, "/", http.StatusMovedPermanently)
}

func (handler todoHandlerImpl) Complete(writer http.ResponseWriter, request *http.Request) {
	id := util.StringToInt(chi.URLParam(request, "id"))
	handler.TodoService.Complete(request.Context(), id)

	http.Redirect(writer, request, "/", http.StatusMovedPermanently)
}
