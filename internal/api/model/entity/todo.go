package entity

type Todo struct {
	Id        int    `db:"id"`
	Item      string `db:"item"`
	Completed bool   `db:"completed"`
}
