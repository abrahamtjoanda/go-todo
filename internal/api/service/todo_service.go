package service

import (
	"context"
	"go-todo/internal/api/model/wrapper"
)

type TodoService interface {
	FindAll(ctx context.Context) []wrapper.TodoResponse
	Create(ctx context.Context, item string)
	Delete(ctx context.Context, id int)
	Complete(ctx context.Context, id int)
}
