package service

import (
	"context"
	"go-todo/internal/api/model/wrapper"
	"go-todo/internal/api/repository"
	"go-todo/internal/util"

	"github.com/jackc/pgx/v5/pgxpool"
)

type todoServiceImpl struct {
	DB             *pgxpool.Pool
	TodoRepository repository.TodoRepository
}

func NewTodoService(db *pgxpool.Pool, todoRepository repository.TodoRepository) TodoService {
	return &todoServiceImpl{
		DB:             db,
		TodoRepository: todoRepository}
}

func (service todoServiceImpl) FindAll(ctx context.Context) []wrapper.TodoResponse {
	tx, err := service.DB.Begin(ctx)
	util.PanicIfError(err)
	defer util.CommitOrRollBack(ctx, tx)

	todos := service.TodoRepository.FindAll(ctx, tx)

	var todoResponses []wrapper.TodoResponse
	for _, todo := range todos {
		todoResponse := wrapper.TodoResponse{
			Id:        todo.Id,
			Item:      todo.Item,
			Completed: todo.Completed,
		}
		todoResponses = append(todoResponses, todoResponse)
	}

	return todoResponses
}

func (service todoServiceImpl) Create(ctx context.Context, item string) {
	tx, err := service.DB.Begin(ctx)
	util.PanicIfError(err)
	defer util.CommitOrRollBack(ctx, tx)

	service.TodoRepository.Create(ctx, tx, item)
}

func (service todoServiceImpl) Delete(ctx context.Context, id int) {
	tx, err := service.DB.Begin(ctx)
	util.PanicIfError(err)
	defer util.CommitOrRollBack(ctx, tx)

	service.TodoRepository.Delete(ctx, tx, id)
}

func (service todoServiceImpl) Complete(ctx context.Context, id int) {
	tx, err := service.DB.Begin(ctx)
	util.PanicIfError(err)
	defer util.CommitOrRollBack(ctx, tx)

	service.TodoRepository.Complete(ctx, tx, id)
}
