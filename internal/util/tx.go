package util

import (
	"context"

	"github.com/jackc/pgx/v5"
)

func CommitOrRollBack(ctx context.Context, tx pgx.Tx) {
	err := recover()
	if err != nil {
		errorRollBack := tx.Rollback(ctx)
		PanicIfError(errorRollBack)
		panic(err)
	} else {
		errorCommit := tx.Commit(ctx)
		PanicIfError(errorCommit)
	}
}
