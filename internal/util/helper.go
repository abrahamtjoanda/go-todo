package util

import (
	"strconv"
)

func PanicIfError(err error) {
	if err != nil {
		panic(err)
	}
}

func StringToInt(str string) int {
	result, err := strconv.Atoi(str)
	PanicIfError(err)

	return result
}

func Unused(x ...any) {}
