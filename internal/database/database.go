package database

import (
	"context"
	"fmt"
	globalconfig "go-todo/internal/config"
	"go-todo/internal/util"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
)

var (
	dbHost     = globalconfig.Getenv("dbHost")
	dbPort     = globalconfig.Getenv("dbPort")
	dbUser     = globalconfig.Getenv("dbUser")
	dbPass     = globalconfig.Getenv("dbPass")
	dbName     = globalconfig.Getenv("dbName")
	dbMinConn  = globalconfig.Getenv("dbMinConn").(int32)
	dbMaxConn  = globalconfig.Getenv("dbMaxConn").(int32)
	dbIdleTime = globalconfig.Getenv("dbIdleTime").(time.Duration)
	dbLifeTime = globalconfig.Getenv("dbLifeTime").(time.Duration)
)

func NewDB() *pgxpool.Pool {
	ctx := context.TODO()

	createDatabaseIfNotExists(ctx)

	databaseURL := fmt.Sprintf("postgres://%s:%s@%s:%s/%s", dbUser, dbPass, dbHost, dbPort, dbName)
	config, err := pgxpool.ParseConfig(databaseURL)
	util.PanicIfError(err)

	config.MinConns = dbMinConn
	config.MaxConns = dbMaxConn
	config.MaxConnIdleTime = dbIdleTime
	config.MaxConnLifetime = dbLifeTime

	db, err := pgxpool.NewWithConfig(ctx, config)
	util.PanicIfError(err)

	err = db.Ping(ctx)
	if err != nil {
		panic(err)
	} else {
		fmt.Println("database connection successful")
	}

	createTableIfNotExists(ctx, db)

	fmt.Println("application is ready to use")

	return db
}

func createDatabaseIfNotExists(ctx context.Context) {
	databaseURL := fmt.Sprintf("postgres://%s:%s@%s:%s", dbUser, dbPass, dbHost, dbPort)
	db, err := pgxpool.New(ctx, databaseURL)
	util.PanicIfError(err)

	sql := `select datname from pg_catalog.pg_database where datname = $1`
	rows, err := db.Query(ctx, sql, dbName)
	util.PanicIfError(err)

	if !rows.Next() {
		fmt.Println("database is not found, setting up database...")
		sql = fmt.Sprintf(`create database %s`, dbName)
		_, err := db.Exec(ctx, sql)
		util.PanicIfError(err)
		fmt.Println("database is created")
	} else {
		fmt.Println("database is found")
	}

	rows.Close()
	db.Close()
}

func createTableIfNotExists(ctx context.Context, db *pgxpool.Pool) {
	tableName := "todos"
	sql := `select tablename from pg_tables where tablename = $1`
	rows, err := db.Query(ctx, sql, tableName)
	util.PanicIfError(err)
	defer rows.Close()

	if !rows.Next() {
		fmt.Println("table is not found, setting up table...")
		sql = fmt.Sprintf(`
			create table %s (
				id serial primary key,
				item text not null,
				completed bool default false
			)`, tableName)
		_, err = db.Exec(ctx, sql)
		util.PanicIfError(err)
		fmt.Println("table is created")
	} else {
		fmt.Println("table is found")
	}
}
