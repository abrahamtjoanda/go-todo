package server

import (
	"go-todo/internal/api/handler"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

func NewRouter(todoHandler handler.TodoHandler) chi.Router {
	router := chi.NewRouter()
	router.Use(middleware.Logger)
	router.Get("/", todoHandler.Show)
	router.Post("/add", todoHandler.Create)
	router.Get("/delete/{id}", todoHandler.Delete)
	router.Get("/complete/{id}", todoHandler.Complete)

	return router
}
