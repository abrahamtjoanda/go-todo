package server

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

func NewServer(router chi.Router) {
	server := http.Server{
		Addr:    "localhost:3000",
		Handler: router,
	}

	server.ListenAndServe()
}
