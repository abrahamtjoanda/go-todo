package config

import (
	"os"
	"time"

	"go-todo/internal/util"

	_ "github.com/joho/godotenv/autoload"
)

var config = map[string]any{
	"dbHost":     os.Getenv("DB_HOST"),
	"dbPort":     os.Getenv("DB_PORT"),
	"dbUser":     os.Getenv("DB_USER"),
	"dbPass":     os.Getenv("DB_PASS"),
	"dbName":     os.Getenv("DB_NAME"),
	"dbMinConn":  int32(util.StringToInt(os.Getenv("DB_MIN_CONN"))),
	"dbMaxConn":  int32(util.StringToInt(os.Getenv("DB_MAX_CONN"))),
	"dbIdleTime": time.Duration(util.StringToInt(os.Getenv("DB_IDLE_TIME"))) * time.Minute,
	"dbLifeTime": time.Duration(util.StringToInt(os.Getenv("DB_LIFE_TIME"))) * time.Minute,
}

func Getenv(key string) any {
	return config[key]
}
