#make build
build:
	clear
	go build -C ./bin ../cmd/main.go

#make start
start:
	clear
	./bin/main.exe

#make buildandstart
buildandstart:
	clear
	go build -C ./bin ../cmd/main.go
	./bin/main.exe

#make run
run:
	clear
	go run cmd/main.go

#make testunit
testunit:
	clear
	go clean -testcache
	go test -v ./test

#make testunitsub sub="<subtestname>"
testunitsub:
	clear
	go clean -testcache
	go test -v ./test -run TestRepositoryTodo/${sub}