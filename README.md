# Go Todo

![todo](resources/todo.png)

## Introduction

A simple todo list application in Go-Lang

## Requirements

- PostgresSQL installed
- Go installed
- Makefile Optional

## Installation

- Clone this repo

```bash
git clone https://gitlab.com/abrahamtjoanda/go-todo.git
```

- Change Directory

```bash
cd go-todo
```

- Initiate `.env` file

```bash
cp .env.example .env
```

- Modify `.env` file with your correct database credentials and desired Port

## Build

To build this application, execute:

```bash
go build -C ./bin ../cmd/main.go
```

or

```bash
make build
```

## Usage

To run this application, execute:

```bash
./bin/main.exe
```

or

```bash
make start
```

or

```bash
make buildandstart
```

You should be able to access this application at `http://127.0.0.1:8080`

> **NOTE**<br>
> If you modified the port in the `.env` file, you should access the application for the port you set

## Test

To test this application, execute:

```bash
go clean -testcache
go test -v ./test
```

or

```bash
make testunit
```

## Credits

This project is built based on the idea of [ichtrojan/go-todo](https://github.com/ichtrojan/go-todo) as a reference
