package test

import (
	"context"
	"fmt"
	"go-todo/internal/util"
	"os"
	"time"

	"github.com/jackc/pgx/v5/pgxpool"
)

type config struct {
	dbHost     string
	dbPort     string
	dbUser     string
	dbPass     string
	dbName     string
	dbMinConn  int32
	dbMaxConn  int32
	dbLifeTime time.Duration
	dbIdleTime time.Duration
}

const tableName = "todos"

func setupTestDB() *pgxpool.Pool {
	ctx := context.TODO()

	cfg := config{
		dbHost:     os.Getenv("DB_HOST"),
		dbPort:     os.Getenv("DB_PORT"),
		dbUser:     os.Getenv("DB_USER"),
		dbPass:     os.Getenv("DB_PASS"),
		dbName:     os.Getenv("DB_NAME_TEST"),
		dbMinConn:  int32(util.StringToInt(os.Getenv("DB_MIN_CONN"))),
		dbMaxConn:  int32(util.StringToInt(os.Getenv("DB_MAX_CONN"))),
		dbIdleTime: time.Duration(util.StringToInt(os.Getenv("DB_IDLE_TIME"))) * time.Minute,
		dbLifeTime: time.Duration(util.StringToInt(os.Getenv("DB_LIFE_TIME"))) * time.Minute,
	}

	createDatabaseIfNotExists(ctx, cfg)

	databaseURL := fmt.Sprintf("postgres://%s:%s@%s:%s/%s", cfg.dbUser, cfg.dbPass, cfg.dbHost, cfg.dbPort, cfg.dbName)
	config, err := pgxpool.ParseConfig(databaseURL)
	util.PanicIfError(err)

	config.MinConns = cfg.dbMinConn
	config.MaxConns = cfg.dbMaxConn
	config.MaxConnIdleTime = cfg.dbIdleTime
	config.MaxConnLifetime = cfg.dbLifeTime

	db, err := pgxpool.NewWithConfig(ctx, config)
	util.PanicIfError(err)

	err = db.Ping(ctx)
	if err != nil {
		panic(err)
	} else {
		fmt.Println("test database connection successful")
	}

	createTableIfNotExists(ctx, db)

	fmt.Println("test application is ready to use")

	return db
}

func createDatabaseIfNotExists(ctx context.Context, cfg config) {
	databaseURL := fmt.Sprintf("postgres://%s:%s@%s:%s", cfg.dbUser, cfg.dbPass, cfg.dbHost, cfg.dbPort)
	db, err := pgxpool.New(ctx, databaseURL)
	util.PanicIfError(err)

	sql := `select datname from pg_catalog.pg_database where datname = $1`
	rows, err := db.Query(ctx, sql, cfg.dbName)
	util.PanicIfError(err)

	if !rows.Next() {
		fmt.Println("test database is not found, setting up test database...")
		sql = fmt.Sprintf(`create database %s`, cfg.dbName)
		_, err := db.Exec(ctx, sql)
		util.PanicIfError(err)
		fmt.Println("test database is created")
	} else {
		fmt.Println("test database is found")
	}

	rows.Close()
	db.Close()
}

func createTableIfNotExists(ctx context.Context, db *pgxpool.Pool) {
	sql := `select tablename from pg_tables where tablename = $1`
	rows, err := db.Query(ctx, sql, tableName)
	util.PanicIfError(err)
	defer rows.Close()

	if !rows.Next() {
		fmt.Println("test table is not found, setting up test table...")
		sql = fmt.Sprintf(`
			create table %s (
				id serial primary key,
				item text not null,
				completed bool default false
			)`, tableName)
		_, err = db.Exec(ctx, sql)
		util.PanicIfError(err)
		fmt.Println("test table is created")
	} else {
		fmt.Println("test table is found")
	}
}

func truncateTable(ctx context.Context, db *pgxpool.Pool, tableName string) {
	sql := fmt.Sprintf("truncate table %s restart identity", tableName)
	_, err := db.Exec(ctx, sql)
	util.PanicIfError(err)
}
