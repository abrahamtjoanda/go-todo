package test

import (
	"fmt"
	"path/filepath"
	"testing"

	"github.com/joho/godotenv"
)

func TestMain(m *testing.M) {
	fmt.Println("Load Env...")
	godotenv.Load(filepath.Join("../", ".env"))

	m.Run()
}
