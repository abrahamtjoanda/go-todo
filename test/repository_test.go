package test

import (
	"go-todo/internal/api/repository"
)

func setupTestRepository() repository.TodoRepository {
	return repository.NewTodoRepository()
}
