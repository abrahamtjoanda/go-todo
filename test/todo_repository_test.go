package test

import (
	"context"
	"errors"
	"fmt"
	"go-todo/internal/api/model/entity"
	"go-todo/internal/api/repository"
	"go-todo/internal/util"
	"sort"
	"testing"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/stretchr/testify/assert"
)

func TestRepositoryTodo(t *testing.T) {
	ctx := context.TODO()
	db := setupTestDB()
	repository := setupTestRepository()
	t.Run("create todo", func(t *testing.T) { createTodoTest(t, ctx, db, repository, "Clean Trash") })
	t.Run("create todo 2", func(t *testing.T) { createTodoTest(t, ctx, db, repository, "Do Laundry") })
	t.Run("find all todo", func(t *testing.T) { findAllTodoTest(t, ctx, db, repository) })
	t.Run("delete todo", func(t *testing.T) { deleteTodoTest(t, ctx, db, repository) })
	t.Run("complete todo", func(t *testing.T) { completeTodoTest(t, ctx, db, repository) })

}

func findItemById(ctx context.Context, tx pgx.Tx, id int) (entity.Todo, error) {
	sql := fmt.Sprintf(`select id, item, completed from %s where id = $1`, tableName)
	rows, _ := tx.Query(ctx, sql, id)
	defer rows.Close()

	var todo entity.Todo

	if rows.Next() {
		err := rows.Scan(&todo.Id, &todo.Item, &todo.Completed)
		util.PanicIfError(err)

		return todo, nil
	} else {
		return todo, errors.New("item is not found")
	}
}

func createTodoTest(t *testing.T, ctx context.Context, db *pgxpool.Pool, repository repository.TodoRepository, item string) {
	truncateTable(ctx, db, tableName)

	tx, err := db.Begin(ctx)
	util.PanicIfError(err)
	todo := repository.Create(ctx, tx, item)

	result, err := findItemById(ctx, tx, todo.Id)
	util.CommitOrRollBack(ctx, tx)

	assert.Nil(t, err)
	assert.Equal(t, todo, result)
}

func findAllTodoTest(t *testing.T, ctx context.Context, db *pgxpool.Pool, repository repository.TodoRepository) {
	truncateTable(ctx, db, tableName)

	var todos []entity.Todo
	testDataCount := 3
	testDataLength := 10
	tx, err := db.Begin(ctx)
	util.PanicIfError(err)
	for i := 0; i < testDataCount; i++ {
		item := randomString(testDataLength)
		todo := repository.Create(ctx, tx, item)
		todos = append(todos, todo)
	}

	result := repository.FindAll(ctx, tx)
	util.CommitOrRollBack(ctx, tx)

	sort.Slice(todos, func(i, j int) bool {
		return todos[j].Id < todos[i].Id
	})

	assert.Equal(t, todos, result)
}

func deleteTodoTest(t *testing.T, ctx context.Context, db *pgxpool.Pool, repository repository.TodoRepository) {
	truncateTable(ctx, db, tableName)

	testDataLength := 10
	tx, err := db.Begin(ctx)
	util.PanicIfError(err)
	item := randomString(testDataLength)
	todo := repository.Create(ctx, tx, item)

	result, err := findItemById(ctx, tx, todo.Id)
	assert.Nil(t, err)
	assert.Equal(t, todo, result)

	repository.Delete(ctx, tx, todo.Id)

	result, err = findItemById(ctx, tx, todo.Id)
	util.CommitOrRollBack(ctx, tx)

	expected := entity.Todo{}

	assert.NotNil(t, err)
	assert.Equal(t, err, errors.New("item is not found"))
	assert.Equal(t, expected, result)
}

func completeTodoTest(t *testing.T, ctx context.Context, db *pgxpool.Pool, repository repository.TodoRepository) {
	truncateTable(ctx, db, tableName)

	testDataLength := 10
	tx, err := db.Begin(ctx)
	util.PanicIfError(err)
	item := randomString(testDataLength)
	todo := repository.Create(ctx, tx, item)

	result, err := findItemById(ctx, tx, todo.Id)
	assert.Nil(t, err)
	assert.Equal(t, todo, result)

	repository.Complete(ctx, tx, todo.Id)

	result, err = findItemById(ctx, tx, todo.Id)
	util.CommitOrRollBack(ctx, tx)

	expected := entity.Todo{
		Id:        todo.Id,
		Item:      todo.Item,
		Completed: true,
	}
	assert.Nil(t, err)
	assert.Equal(t, expected, result)

}
