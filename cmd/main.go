package main

import (
	"go-todo/internal/api/handler"
	"go-todo/internal/api/repository"
	"go-todo/internal/api/service"
	"go-todo/internal/database"
	"go-todo/internal/server"
)

func main() {
	db := database.NewDB()
	todoRepository := repository.NewTodoRepository()
	todoService := service.NewTodoService(db, todoRepository)
	todoHandler := handler.NewTodoHandler(todoService)

	router := server.NewRouter(todoHandler)
	server.NewServer(router)
}
